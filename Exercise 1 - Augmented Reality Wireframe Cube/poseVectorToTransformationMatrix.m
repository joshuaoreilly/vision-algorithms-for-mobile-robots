function T = poseVectorToTransformationMatrix(pose)
    omega = pose(1,1:3);
    t = pose(1,4:6);
    
    theta = norm(omega);
    k = omega./theta;
    
    k_x = [0, -k(3), k(2);
            k(3), 0, -k(1);
            -k(2), k(1), 0];
    
    R = eye(3) + sin(theta) * k_x + (1 - cos(theta)) * k_x * k_x;
    
    % ' is transpose
    T = [R t'];
end

