function [u_d,v_d] = projectPoints(K, D, T, P_w)
    P_c = T * [P_w ; 1];
    
    % lambda is just Z_c
    lambda = P_c(3);
    
    uv = (K * P_c) ./ lambda;
    
    u = uv(1);
    v = uv(2);
    
    % u0 and v0 the optical center of the image
    % since image is 752 x 480 pixels, the center is at 376 x 240
    u0 = 376;
    v0 = 240;
    
    r2 = (u - u0)^2 + (v - v0)^2;
    
    u_d = (1 + D(1)*r2 + D(2)*(r2^2)) * (u-u0) + u0;
    v_d = (1 + D(1)*r2 + D(2)*(r2^2)) * (v-v0) + v0;
end

