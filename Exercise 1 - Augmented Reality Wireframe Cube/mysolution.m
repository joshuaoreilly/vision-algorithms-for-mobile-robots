K = readmatrix('data/K.txt');
D = dlmread('data/D.txt');

distorted = imread('data/images/img_0001.jpg');
undistorted_grayscale = rgb2gray(distorted);

[X_w,Y_w,Z_w] = meshgrid(0:0.04:0.32, 0:0.04:0.20, 0);

meshpoints = [X_w(:) Y_w(:) Z_w(:)];

poses = readlines('data/poses.txt');
pose = str2num(poses(1));

T = poseVectorToTransformationMatrix(pose);

figure, imshow(distorted, [])
hold on

for i = 1:size(meshpoints)
    [u, v] = projectPoints(K, D, T, meshpoints(i,:)');
    scatter(u,v, 100, 'filled')
end

% number of grids large, multiples of 0.04m
width = 2;

% location of the top left corner of the base of the cube
top_left = [0.08 0.08];
vertices = [top_left(1) top_left(2) 0;
            top_left(1)+width*0.04 top_left(2) 0;
            top_left(1) top_left(2)+width*0.04 0;
            top_left(1)+width*0.04 top_left(2)+width*0.04 0;
            top_left(1) top_left(2) -width*0.04;
            top_left(1)+width*0.04 top_left(2) -width*0.04;
            top_left(1) top_left(2)+width*0.04 -width*0.04;
            top_left(1)+width*0.04 top_left(2)+width*0.04 -width*0.04];

% I'm lazy, lets just draw lines between every vertex
vertices_projected = zeros(8,2);
for i = 1:size(vertices)
     [x, y] = projectPoints(K, D, T, vertices(i,:)');
     vertices_projected(i,1) = x;
     vertices_projected(i,2) = y;
end

% base square
plot([vertices_projected(1,1) vertices_projected(2,1)], [vertices_projected(1,2) vertices_projected(2,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(1,1) vertices_projected(3,1)], [vertices_projected(1,2) vertices_projected(3,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(2,1) vertices_projected(4,1)], [vertices_projected(2,2) vertices_projected(4,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(3,1) vertices_projected(4,1)], [vertices_projected(3,2) vertices_projected(4,2)], '-r', 'LineWidth', 2.0)

% vertical edges
plot([vertices_projected(1,1) vertices_projected(5,1)], [vertices_projected(1,2) vertices_projected(5,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(2,1) vertices_projected(6,1)], [vertices_projected(2,2) vertices_projected(6,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(3,1) vertices_projected(7,1)], [vertices_projected(3,2) vertices_projected(7,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(4,1) vertices_projected(8,1)], [vertices_projected(4,2) vertices_projected(8,2)], '-r', 'LineWidth', 2.0)

% top square
plot([vertices_projected(5,1) vertices_projected(6,1)], [vertices_projected(5,2) vertices_projected(6,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(5,1) vertices_projected(7,1)], [vertices_projected(5,2) vertices_projected(7,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(6,1) vertices_projected(8,1)], [vertices_projected(6,2) vertices_projected(8,2)], '-r', 'LineWidth', 2.0)
plot([vertices_projected(7,1) vertices_projected(8,1)], [vertices_projected(7,2) vertices_projected(8,2)], '-r', 'LineWidth', 2.0)      
