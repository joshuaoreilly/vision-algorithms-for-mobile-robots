function W = getSimWarp(dx, dy, alpha_deg, lambda)
% alpha given in degrees, as indicated
alpha = deg2rad(alpha_deg);

% warp W defined by the following 2x3 matrix
W = lambda .* [cos(alpha), -sin(alpha), dx; ...
                sin(alpha), cos(alpha), dy];
end