function patch = getWarpedPatch(I, W, x_T, r_T)
% x_T is 1x2 and contains [x_T y_T] as defined in the statement. patch is
% (2*r_T+1)x(2*r_T+1) and arranged consistently with the input image I.

[rows,cols] = size(I);

I_warped = zeros(rows,cols);
% we have to iterate over the entire image, since if the warp is extreme
% enough, pixels from across the entire image might end up in our patch
% region
for y = 1:rows
    for x = 1:cols
        % calculate warp using coordinates relative to the center of the
        % patch
        new_xy_relative = floor(W * [x-x_T(1);y-x_T(2);1]);
        % correct coordinates so that they are relative to the top-left
        % corner again
        new_xy = new_xy_relative + [x_T(1);x_T(2)];
        if (new_xy(2) < rows && new_xy(1) < cols && ...
                0 < new_xy(2) && 0 < new_xy(1))
            new_val = I(y,x);
            %disp(new_xy)
            I_warped(new_xy(2),new_xy(1)) = new_val;
        end
    end
end

x_indices = x_T(1)
patch = I_warped(x_T(1) - 2*r_T+1:)

end