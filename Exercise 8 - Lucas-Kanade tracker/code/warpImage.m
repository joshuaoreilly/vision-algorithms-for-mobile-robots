function I = warpImage(I_R, W)

[rows, cols] = size(I_R);

I = zeros(rows,cols);

for y = 1:rows
    for x = 1:cols
        new_xy = floor(W * [x;y;1]);
        if (new_xy(2) < rows && new_xy(1) < cols && ...
                0 < new_xy(2) && 0 < new_xy(1))
            new_val = I_R(y,x);
            %disp(new_xy)
            I(new_xy(2),new_xy(1)) = new_val;
        end
    end
end

end
