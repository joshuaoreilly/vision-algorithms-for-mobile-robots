function [R,T] = disambiguateRelativePose(Rots,u3,points0_h,points1_h,K1,K2)
% DISAMBIGUATERELATIVEPOSE- finds the correct relative camera pose (among
% four possible configurations) by returning the one that yields points
% lying in front of the image plane (with positive depth).
%
% Arguments:
%   Rots -  3x3x2: the two possible rotations returned by decomposeEssentialMatrix
%   u3   -  a 3x1 vector with the translation information returned by decomposeEssentialMatrix
%   p1   -  3xN homogeneous coordinates of point correspondences in image 1
%   p2   -  3xN homogeneous coordinates of point correspondences in image 2
%   K1   -  3x3 calibration matrix for camera 1
%   K2   -  3x3 calibration matrix for camera 2
%
% Returns:
%   R -  3x3 the correct rotation matrix
%   T -  3x1 the correct translation vector
%
%   where [R|t] = T_C2_C1 = T_C2_W is a transformation that maps points
%   from the world coordinate system (identical to the coordinate system of camera 1)
%   to camera 2.
%

[~, N] = size(points0_h);

R1 = Rots(:,:,1);
R2 = Rots(:,:,2);

trans1 = u3;
trans2 = u3 .* -1;

% transformation matrices
T1 = [R1, trans1];
T2 = [R2, trans1];
T3 = [R1, trans2];
T4 = [R2, trans2];

Tees = cell({T1, T2, T3, T4});

tally = [0,0,0,0];
% iterate over all transformation combinations
for i = 1:length(Tees)
    M1 = K1 * [1,0,0,0;0,1,0,0;0,0,1,0];
    M2 = K2 * Tees{i};
    % compute the number of points in the positive Z axis for each option
    for j = 1:N
        point = linearTriangulation(points0_h(:,j),points1_h(:,j),M1,M2);
        if point(3) > 0
            tally(i) = tally(i) + 1;
        end
    end
end

[~,index] = max(tally);
T_correct = Tees{index};
R = T_correct(1:3,1:3);
T = T_correct(1:3,4);