function [pts_tilda, T] = normalise2dpts(pts)
% NORMALISE2DPTS - normalises 2D homogeneous points
%
% Function translates and normalises a set of 2D homogeneous points
% so that their centroid is at the origin and their mean distance from
% the origin is sqrt(2).
%
% Usage:   [pts_tilda, T] = normalise2dpts(pts)
%
% Argument:
%   pts -  3xN array of 2D homogeneous coordinates
%
% Returns:
%   pts_tilda -  3xN array of transformed 2D homogeneous coordinates.
%   T      -  The 3x3 transformation matrix, pts_tilda = T*pts
%

[rows, cols] = size(pts);

% remove third row, not homogenous anymore
% for whatever reason the points didn't have the third element equal
% to 1, so divide by their third element
pts_euclid = pts(1:2,:) ./ kron([1;1], pts(3,:));
% centroids, sum(p1, 2) gives column vector with each element, thus 
% sum of its row
%mu = sum(pts_euclid, 2) ./ cols;
mu = mean(pts_euclid, 2);

% smean squared magnitude
% sigma = 0;
% for i = 1:cols
%    sigma = sigma + norm(pts_euclid(:,i) - mu);
% end
% 
% sigma = sigma / cols;
%s = sqrt(2) / sigma;
dist = pts_euclid - mu;
s = sqrt(2) / sqrt(1/cols * sum(sum(dist .* dist)));


T = [s, 0, -s*mu(1);
        0, s, -s*mu(2);
        0, 0, 1];
    
% pts_tilda = zeros(rows, cols);
% for i = 1:cols
%    pts_tilda(:,i) = T * pts(:,i);
% end
pts_tilda = T * pts;