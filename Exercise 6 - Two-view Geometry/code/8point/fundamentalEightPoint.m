function F = fundamentalEightPoint(p1,p2)
% fundamentalEightPoint  The 8-point algorithm for the estimation of the fundamental matrix F
%
% The eight-point algorithm for the fundamental matrix with a posteriori
% enforcement of the singularity constraint (det(F)=0).
% Does not include data normalization.
%
% Reference: "Multiple View Geometry" (Hartley & Zisserman 2000), Sect. 10.1 page 262.
%
% Input: point correspondences
%  - p1(3,N): homogeneous coordinates of 2-D points in image 1
%  - p2(3,N): homogeneous coordinates of 2-D points in image 2
%
% Output:
%  - F(3,3) : fundamental matrix
[rows, cols] = size(p1);
Q = zeros(cols, 9);
for i = 1:cols
    Q(i, :) = kron(p1(:, i), p2(:, i))';
end

[~,~,V] = svd(Q);
F_vec = V(:, end);
% convert back from single column vector to matrix form
% while the course notes have a transpose, it's actually incorrect
F = reshape(F_vec, rows, rows);

% NOTE: I didn't do the step where the smallest singular value sigma_3 is
% set to zero, mainly because I couldn't figure out how to modify the svd
% function call to have it.