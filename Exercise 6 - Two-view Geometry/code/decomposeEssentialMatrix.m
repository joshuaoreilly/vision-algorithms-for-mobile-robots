function [R,u3] = decomposeEssentialMatrix(E)
% Given an essential matrix, compute the camera motion, i.e.,  R and T such
% that E ~ T_x R
% 
% Input:
%   - E(3,3) : Essential matrix
%
% Output:
%   - R(3,3,2) : the two possible rotations
%   - u3(3,1)   : a vector with the translation information
[U,~,V] = svd(E);

W = [0, -1, 0; 1, 0, 0; 0, 0, 1];
W_inv = [0, 1, 0; -1, 0, 0; 0, 0, 1];
R1 = U * W * V';
if det(R1) < 0
    R1 = U * W_inv * V';
end
R2 = U*W'*V';
if det(R2) < 0
    R2 = U * (W_inv)' * V';
end
R = cat(3,R1,R2);

% the actual solution might be negative u3, but we'll solve that during teh
% next step
u3 = U(:,end);
