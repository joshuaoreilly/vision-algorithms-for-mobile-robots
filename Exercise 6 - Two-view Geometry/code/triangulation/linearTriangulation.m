function P = linearTriangulation(p1,p2,M1,M2)
% LINEARTRIANGULATION  Linear Triangulation
%
% Input:
%  - p1(3,N): homogeneous coordinates of points in image 1
%  - p2(3,N): homogeneous coordinates of points in image 2
%  - M1(3,4): projection matrix corresponding to first image
%  - M2(3,4): projection matrix corresponding to second image
%
% Output:
%  - P(4,N): homogeneous coordinates of 3-D points
[~,cols] = size(p1);
P = zeros(4,cols);

for i = 1:cols
    left = cross2Matrix(p1(:,i)) * M1;
    right = cross2Matrix(p2(:,i)) * M2;
    [~,~,V] = svd([left;right]);
    % dehomogenize
    P(:,i) = V(:,end) ./ V(end,end);
end
