function kpt_locations = extractKeypoints(DoGs, contrast_threshold)
    num_octaves = numel(DoGs);
    kpt_locations = cell(1, num_octaves);
    for oct_idx = 1:num_octaves
       DoG = DoGs{oct_idx};
       % This is some neat trick to avoid for loops.
       % for each pixel, makes it the highest value of the cubic patch around it
       % if the pixel is unchanged, it is the largest value in the 3x3x3 cube
       % if the pixel has changed, it is because there was a larger value in the 3x3x3 cube
       DoG_max = imdilate(DoG, true(3, 3, 3));
       % Equivalent to this is:
       % DoG_max = movmax(movmax(movmax(DoG, 3, 1), 3, 2), 3, 3);
       is_kpt = (DoG == DoG_max) & (DoG >= contrast_threshold);
       % We do not consider the extrema at the boundaries of the DoGs.
       is_kpt(:, :, 1) = false;
       is_kpt(:, :, end) = false;
       [x, y, s] = ind2sub(size(is_kpt), find(is_kpt));
       kpt_locations{oct_idx} = horzcat(x, y, s);
    end
end