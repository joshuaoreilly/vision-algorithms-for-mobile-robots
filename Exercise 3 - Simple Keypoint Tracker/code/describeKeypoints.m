function descriptors = describeKeypoints(img, keypoints, r)
% Returns a (2r+1)^2xN matrix of image patch vectors based on image
% img and a 2xN matrix containing the keypoint coordinates.
% r is the patch "radius".
N = size(keypoints, 2);
% number of rows = number of elements in a patch
% number of columns = number of keypoints x 2 (x and y coordinates, I
% suppose?)
descriptors = uint8(zeros((2*r + 1) ^ 2, N));
% pad array with zeros so that we can find keypoints along the edges
padded = padarray(img, [r, r]);
for i = 1:N
    % keypoint of column i (since each column is a keypoint)
    kp = keypoints(:, i) + r;
    % reshape each patch into a single line of pixels, which becomes the
    % column corresponding to a given keypoint
    descriptors(:, i) = reshape(...
        padded(kp(1)-r:kp(1)+r, kp(2)-r:kp(2)+r), [], 1);
end