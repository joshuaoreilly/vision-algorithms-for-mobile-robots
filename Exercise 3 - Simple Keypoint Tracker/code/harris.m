function scores = harris(img, patch_size, kappa)
    sobel_x = [-1,0,1;
                -2,0,2;
                -1,0,1];
    sobel_y = [-1,-2,-1;
                0,0,0;
                1,2,1];
    I_dx = conv2(img, sobel_x,'valid');
    I_dy = conv2(img, sobel_y,'valid');
    
    I_dx2  = I_dx .^ 2;
    I_dy2  = I_dy .^2;
    I_dxdy = I_dx .* I_dy;
    
    sum_I_dx2  = conv2(I_dx2,ones(patch_size,patch_size),'valid');
    sum_I_dy2  = conv2(I_dy2,ones(patch_size,patch_size),'valid');
    sum_I_dxdy = conv2(I_dxdy,ones(patch_size,patch_size),'valid');
    
    %disp(size(img))
    %disp(size(I_dx))
    %disp(size(sum_I_dx2))
    
    det = (sum_I_dx2 .* sum_I_dy2) - (sum_I_dxdy .^2);
    trace = sum_I_dx2 + sum_I_dy2;
    
    scores = det - (kappa .* trace);

end