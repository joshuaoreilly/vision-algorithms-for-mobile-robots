function scores = shi_tomasi(img, patch_size)

sobel_x = [-1,0,1;
                -2,0,2;
                -1,0,1];
    sobel_y = [-1,-2,-1;
                0,0,0;
                1,2,1];
    I_dx = conv2(img, sobel_x,'valid');
    I_dy = conv2(img, sobel_y,'valid');
    
    I_dx2  = I_dx .^ 2;
    I_dy2  = I_dy .^2;
    I_dxdy = I_dx .* I_dy;
    
    sum_I_dx2  = conv2(I_dx2,ones(patch_size,patch_size),'valid');
    sum_I_dy2  = conv2(I_dy2,ones(patch_size,patch_size),'valid');
    sum_I_dxdy = conv2(I_dxdy,ones(patch_size,patch_size),'valid');
    
    %disp(size(img))
    %disp(size(I_dx))
    %disp(size(sum_I_dx2))
    
    m_11 = sum_I_dx2;
    m_12 = sum_I_dxdy;
    m_21 = sum_I_dxdy;
    m_22 = sum_I_dy2;
    
    lambda_1 = 0.5 .* ((m_11+m_22)+sqrt(4.*m_12.*m_21 + (m_11-m_22).^2));
    lambda_2 = 0.5 .* ((m_11+m_22)-sqrt(4.*m_12.*m_21 + (m_11-m_22).^2));
    scores = min(lambda_1,lambda_2);
end