function keypoints = selectKeypoints(scores, num, r)
% Selects the num best scores as keypoints and performs non-maximum 
% supression of a (2r + 1)*(2r + 1) box around the current maximum.
    keypoints = zeros(2, num);
    % pad the array with zeros so that we can find best scores on edges
    temp_scores = padarray(scores, [r r]);
    for i = 1:num
        % return indices of max element (ignore actual val)
        [~, kp] = max(temp_scores(:));
        % get index where max of the temp scores are found
        [row, col] = ind2sub(size(temp_scores), kp);
        % the default index notation used by max() is weird, so here, we're
        % converting it back to row and column
        kp = [row;col];
        % since we padded left and top with r zeros, need to remove r from
        % the index
        % adds the given point to the keypoints
        keypoints(:,i) = kp - r;
        % since we don't want to reuse the same points, turn the patch we
        % just found a keypoint for into a bunch of zeros
        temp_scores(kp(1)-r:kp(1)+r, kp(2)-r:kp(2)+r) = ...
            zeros(2*r + 1, 2*r + 1);
    end
end
