clear all
close all
clc

num_scales = 3; % Scales per octave.
num_octaves = 5; % Number of octaves.
sigma = 1.6;
contrast_threshold = 0.04;
image_file_1 = 'images/img_1.jpg';
image_file_2 = 'images/img_2.jpg';
rescale_factor = 0.2; % Rescaling of the original image for speed.

images = {getImage(image_file_1, rescale_factor),...
    getImage(image_file_2, rescale_factor)};

kpt_locations = cell(1, 2);
descriptors = cell(1, 2);

for img_idx = 1:2
    % Write code to compute:
    % 1)    image pyramid. Number of images in the pyarmid equals
    %       'num_octaves'.
    % new cell array with initial image
    im_pyramid = cell(1,num_octaves);
    im_pyramid{1} = images{img_idx};
    for i = 2:num_octaves
        im_pyramid{i} = impyramid(im_pyramid{i-1}, 'reduce');
    end
    % 2)    blurred images for each octave. Each octave contains
    %       'num_scales + 3' blurred images.
    for i = 1:num_octaves
        ref_img = im_pyramid{i};
        [x,y,z] = size(im_pyramid{i});
        im_pyramid{i} = zeros(x,y,z);
        for j = 1:num_scales+3
            % initial s should be -1
            s = j - 2;
            sigma_scaled = 2^(s / num_scales);
            im_pyramid{i}(:,:,j) = imgaussfilt(ref_img, sigma_scaled);
        end
    end
    % 3)    'num_scales + 2' difference of Gaussians for each octave.
    dog_pyramid = cell(1,num_octaves);
    for i = 1:num_octaves
        [x,y,z] = size(im_pyramid{i});
        dog_pyramid{i} = zeros(x,y,z-1);
        for j = 1:num_scales+2
            dog_pyramid{i}(:,:,j) = im_pyramid{i}(:,:,j) - im_pyramid{i}(:,:,j+1);
        end
    end
    % 4)    Compute the keypoints with non-maximum suppression and
    %       discard candidates with the contrast threshold.
    for i = 1:num_octaves
        dog_pyramid{i}(dog_pyramid{i} < contrast_threshold) = 0;
    end
    % 5)    Given the blurred images and keypoints, compute the
    %       descriptors. Discard keypoints/descriptors that are too close
    %       to the boundary of the image. Hence, you will most likely
    %       lose some keypoints that you have computed earlier.
    for i = 1:num_octaves
        [x,y,z] = size(im_pyramid{i});
        % don't iterate over first and last DoG in each octave
        for j = 2:num_scales+2-1
            for k = 2:x-1
                for l = 2:y-1
                    
                end
            end
        end
        % supress keypoints in top and bottom scales of octave
        dog_pyramid{i}(:,:,1) = 0;
        dog_pyramid{i}(:,:,end) = 0;
    end
end
% Finally, match the descriptors using the function 'matchFeatures' and
% visualize the matches with the function 'showMatchedFeatures'.
% If you want, you can also implement the matching procedure yourself using
% 'knnsearch'.