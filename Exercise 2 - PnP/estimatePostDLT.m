function M = estimatePoseDLT(p, P, K)
	% p = pixel coordinates [u, v, 1]^T of point (not normalized calibrated coordinates K^(-1) [u,v,1]^T)
	% P = world coordinates [X_w, Y_w, Z_w, 1]^T of point
	% K = intrinsic parameter matrix containing alpha = pixels/meter and focal length

	% Step 1: get calibrated/normalized pixel coordinates:

	p_calibrated = inv(K) * p;

	% Step 2: build Q (code taken from Isar, it's magic
	Q = []
	for i = 1:N
		P_temp = [P(i, :) 1];
		% Left-hand side of Q
		LHS = kron(eye(2), P_temp);
		% Right-hand side of Q
		xy = -xy1(1,2,i);
		RHS = kron(xy, P_temp)
		Q = [Q; [LHS RHS]];

	% Step 3: solve for M_tilde subject to constraint ||M_tilde|| = 1 (from matlab documentation)
	[~,~,V] = svd(Q); % ~ means we don't care about the value
	M_tilde = V(:,12);
	
	% since the entire matrix was spread out into 12x1 before
	% transpose because matlab reshapes it column-wise, not row-wise
	M_tilde = reshape(M_tilde, 4, 3)';

	% must ensure the translation in z is positive
	% otherwise, the pixel would be behind the camera
	if M_tilde(3,4) < 0
		M_tilde = -1 .* M_tilde;
	end

	% Note: not sure what they were trying to achieve here, but I'll copy it
	% a negative determinant implies something?
	if det(M_tilde(:,1:3)) < 0
		M_tilde = - M_tilde;
	end
	
	R = M_tilde(:,1:3);

	% R has no guarantee to belong to the space of rotation matrices (SO(3), which have unique properties)
	% So we solve the Orthogonal Procrustes Problem
	[U,~,V] = svd(R);
	R_tilde = U*V';

	% Since R_tilde = alpha * R, we can get the missing scaling factor alpha
	% 'fro' = Frobenius norm, or 2-norm for m x n matrices instead of N x 1 vectors
	alpha = norm(R_tilde, 'fro') / norm(R, 'fro');

	% Build M_tilde with corrected rotation and scale
	M_tilde = [R_tilde alpha * M_tilde(:,4)];

end
