function p_reproj = reprojectPoints(P, M_tilde, K)
	% reproject 3D points given a projection matrix (refines the estimation by minimizing error)
	% P: [nx3] coordinates of teh 3D points in the world frame (all of them, I guess?)
	% M_tilde: [3x4] projection matrix
	% K: [3x3] camera matrix (calibration matrix?)
	%
	% p_reproj: [nx2] coordinates of the reprojected 2D points

	% In the notes, it is indicated that we can solve this by using teh Levenberg-Marquardt
	% algorithm to minimize the error bewteen projected points and actual point positions on the
	% image plane (it's also known as damped least squares, so minimzing error for multivariable
	% problem)
	%
	% All they did is divide element-wise by the focal length (or p_z)?
	p_homo = (K * M_tilde * [P; ones(1, lenth(P))])';
	p_homo(:,1) = p_homo(:,1) ./ p_homo(:,3);
	p_homo(:,2) = p_homo(:,2) ./ p_homo(:,3);

	p_reproj = p_homo(:, 1:2);
end
